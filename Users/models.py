from django.db import models
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField


class user(models.Model):
    name = models.CharField(max_length=100)
    registerDate = models.DateTimeField(default=timezone.now)
    phoneNumber = PhoneNumberField()

    def __str__(self):
        return str(self.id) + ' ==> ' + self.name + ' : ' + str(self.phoneNumber)
