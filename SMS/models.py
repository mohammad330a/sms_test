import datetime

from django.db import models
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField
from .kave import send_sms


class log(models.Model):
    code = models.CharField(max_length=10)
    date = models.DateTimeField(default=timezone.now)
    phoneNumber = PhoneNumberField()
    detail = models.CharField(max_length=200, blank=True)

    def save(self, *args, **kwargs):
        self.detail = str(send_sms(self.code, str(self.phoneNumber)))
        f = open('/home/mohammad330a/Desktop/test.txt', "a")
        f.write(f"pls GOD pls : {self.date} ==> {self.phoneNumber} ==> {self.code} ==> {self.detail} \n")
        f.close()
        super().save(*args, **kwargs)  # Call the "real" save() method.
