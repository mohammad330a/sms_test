from rest_framework import serializers

from .models import log


class logSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = log
        fields = ('code', 'date', 'phoneNumber')
