from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

from rest_framework import viewsets
from .serializers import logSerializer
from .models import log


class logViewSet(viewsets.ModelViewSet):
    queryset = log.objects.all().order_by('id')
    serializer_class = logSerializer


def index(request):
    #log.objects.create(code='', phoneNumber='+989368777523')
    return HttpResponse('Hello')

